#include<stdio.h>
#include<unistd.h>
#include<termios.h>
#include<string>
#include "vehicle/can_obj.h"
#include "can_transmitter/can_transmitter.h"

//for localization
#include <thread>
#include <chrono>
#include <iostream>
#include <vector>
#include <functional>
#include <iomanip>
#include "localization/localization.h"



using namespace PIAUTO::chassis;
using namespace PIAUTO::time;


int main(int argc,char *argv[]) {
    CanObj::InitFromPresets(Chassis_Type::POD5);
    CanObj *CO = CanObj::GetCanObj();


    /// After starting the program, you must first switch to automatic driving mode
    /// before you can control the vehicle,default is manual mode.
    //CO->GetCommandCtrl().ExcCommand(CommandType::SetDriveMode,0);

    CanTransmitter can_transmitter_;
    VCI_CAN_OBJ temp;

    std::string addr = "tcp://127.0.0.1";
    addr = addr + ":5555";

    auto localization = std::make_shared<PIAUTO::localization::VIGLocalization>(addr);
    bool ret = localization->Init();
    if(!ret){
      std::cout << "Init localization client failed! \n";
      //return 1;
    }
    
    unsigned int dataUTM_x;
    unsigned int dataUTM_y;
    unsigned int timeStamp;
    char ID_xy[]{"7F1"};        // ID in hex for datav x and y
    char ID_timestamp[]{"7F2"}; // ID in hex for time stamp
    char ID_utmzone[]{"7F3"};   // ID in hex for zone
    bool isSent;



    //temp = can_transmitter_.GenerateFrame(ID,"11",0,0,0);  //setup frame
    //temp.DataLen  = 8;


    while(true) {

      PIAUTO::localization::LocalizationData data;
      bool result = localization->GetLocalization(data);
      if(!result){
        std::cout << "GetLocalization error!\n";
      }

      //std::cout <<"data.utm_x    " << data.utm_x << "\n";
      //std::cout <<"data.utm_y    " << data.utm_y << "\n";
            
      //std::string dataString_x = std::to_string(data.utm_x);
      //std::string dataString_x = std::to_string(prova);
	  
	  
      //dataUTM_x = data.utm_x * 100; // centimeter precision
      //dataUTM_y = data.utm_y * 100; // centimeter precision

      double test_utm_x = 359614.42;
      double test_utm_y = 5183790.98;
      dataUTM_x = test_utm_x * 100;
      dataUTM_y = test_utm_y * 100;

      
          
      
      //CAN frame setup for DATA

      temp = can_transmitter_.GenerateFrame(ID_xy,"11",0,0,0);  //setup frame
      temp.DataLen  = 8;


      temp.Data[7] = (dataUTM_y & 0x000000ff);
      temp.Data[6] = (dataUTM_y & 0x0000ff00) >> 8;
      temp.Data[5] = (dataUTM_y & 0x00ff0000) >> 16;
      temp.Data[4] = (dataUTM_y & 0xff000000) >> 24;

      temp.Data[3] = (dataUTM_x & 0x000000ff);
      temp.Data[2] = (dataUTM_x & 0x0000ff00) >> 8;
      temp.Data[1] = (dataUTM_x & 0x00ff0000) >> 16;
      temp.Data[0] = (dataUTM_x & 0xff000000) >> 24;

      printf("%d",temp.Data[0]);
      std::cout << temp.Data[0] << "\n";
      std::cout << temp.Data[1] << "\n";
      std::cout << temp.Data[2] << "\n";
      std::cout << temp.Data[3] << "\n\n\n\n";

      isSent = can_transmitter_.SendData(&temp);
        std::cout << "Failed to sent message on CAN bus! \n";
      if(!isSent){
      }
      
      
      double test_timestamp = 093812.05;
      timeStamp = test_timestamp * 100;
      
      //CAN frame setup for GPS Time Stamp in seconds

      temp = can_transmitter_.GenerateFrame(ID_timestamp,"11",0,0,0);  //setup frame
      temp.DataLen  = 8;
      
      temp.Data[7] = (timeStamp & 0x00000000000000ff);
      temp.Data[6] = (timeStamp & 0x000000000000ff00) >> 8;
      temp.Data[5] = (timeStamp & 0x0000000000ff0000) >> 16;
      temp.Data[4] = (timeStamp & 0x00000000ff000000) >> 24;

      temp.Data[3] = (timeStamp & 0x000000ff00000000) >> 32;
      temp.Data[2] = (timeStamp & 0x0000ff0000000000) >> 40;
      temp.Data[1] = (timeStamp & 0x00ff000000000000) >> 48;
      temp.Data[0] = (timeStamp & 0xff00000000000000) >> 56;


      isSent = can_transmitter_.SendData(&temp);
      if(!isSent){
        std::cout << "Failed to sent message on CAN bus! \n";
      }

      //CAN frame setup for UTM zone

      temp = can_transmitter_.GenerateFrame(ID_utmzone,"11",0,0,0);  //setup frame
      temp.DataLen  = 8;

      std::cout << data.utm_zone;
      data.utm_zone = 32;

      temp.Data[7] = (data.utm_zone & 0x000000ff);
      temp.Data[6] = (data.utm_zone & 0x0000ff00) >> 8;
      temp.Data[5] = (data.utm_zone & 0x00ff0000) >> 16;
      temp.Data[4] = (data.utm_zone & 0xff000000) >> 24;

      temp.Data[3] = 0;
      temp.Data[2] = 0;
      temp.Data[1] = 0;
      temp.Data[0] = 0;



      isSent = can_transmitter_.SendData(&temp);
      if(!isSent){
        std::cout << "Failed to sent message on CAN bus! \n";
      }


    usleep(1000*200);   // Every 200 ms
    }
    return 0;
}
