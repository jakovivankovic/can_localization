/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/


#ifndef ADU_CLIENT_CHASSIS_CONFIG_H
#define ADU_CLIENT_CHASSIS_CONFIG_H

#endif  // ADU_CLIENT_CHASSIS_CONFIG_H

#include <glog/logging.h>
#include <yaml-cpp/yaml.h>

namespace PIAUTO {
namespace chassis {

enum Chassis_Type : char {
  S8 = 0,
  POD1 = 1,
  POD4 = 4,
  POD5 = 5,
  POD6 = 6,
  VENDING2 = 16
};

static const std::array<Chassis_Type, 6> kTypeIds = {
    {S8, POD1, POD4, POD5, POD6, VENDING2}};

static const std::array<std::string, 6> kTypeNames = {
    {"S8", "POD1", "POD4", "POD5", "POD6", "VENDING2"}};
/**
 * @brief Convert chassis Id to name.
 * @param chassis_type chssis type id.
 * @return Chassis name.
 */
static std::string ConvertChassisIdToName(const Chassis_Type chassis_type) {
  for (size_t i = 0; i < kTypeIds.size(); i++) {
    if (kTypeIds.at(i) == chassis_type) {
      return kTypeNames.at(i);
    }
  }

  LOG(ERROR) << "[chassis] Unrecognized chassis type";
  return "";
}

/**
 * @brief Convert chassis name to Id.
 * @param name Chassis's name by PerceptIn named.
 * @return Chassis_type ID.
 */
static Chassis_Type ConvertChassisNameToId(const std::string &name) {
  for (size_t i = 0; i < kTypeNames.size(); i++) {
    if (kTypeNames.at(i) == name) {
      return kTypeIds.at(i);
    }
  }

  LOG(ERROR) << "[chassis] Unrecognized chassis name: [" << name << "]";
  return kTypeIds.front();
}

/**
 * @class ChassisConfig
 * @brief Init chassis config information.
 */
class ChassisConfig {
 public:
  ChassisConfig(Chassis_Type chassis_type) {
    switch (chassis_type) {
      case Chassis_Type::S8:
        vcu_type_ = 3;
        log_path_ = "./log";
        radar_number_ = 8;
        sonar_channel_index_ = 0;
        sonar_channel_num_ = 4;
        break;

      case Chassis_Type::POD1:
        vcu_type_ = 0;
        log_path_ = "./log";
        radar_number_ = 6;
        sonar_channel_index_ = 1;
        sonar_channel_num_ = 2;
        break;

      case Chassis_Type::POD4:
        vcu_type_ = 3;
        log_path_ = "./log";
        radar_number_ = 6;
        sonar_channel_index_ = 1;
        sonar_channel_num_ = 2;
        break;

      case Chassis_Type::POD5:
      case Chassis_Type::POD6:
        vcu_type_ = 3;
        log_path_ = "./log";
        radar_number_ = 6;
        sonar_channel_index_ = 0;
        sonar_channel_num_ = 4;
        break;

      case Chassis_Type::VENDING2:
        vcu_type_ = 1;
        log_path_ = "./log";
        radar_number_ = 6;
        sonar_channel_index_ = 1;
        sonar_channel_num_ = 2;
        break;

      default:
        break;
    }
  };

  ChassisConfig(){};

  void ParseFromYaml(const YAML::Node &node) {
    vcu_type_ = node["vcu_type"].as<int>();
    log_path_ = node["log_path"].as<std::string>();
    radar_number_ = node["radar"]["num"].as<int>();
    sonar_channel_num_ = node["sonar"]["channel_num"].as<int>();
    sonar_channel_index_ = node["sonar"]["channel_index"].as<int>();
  }

  const uint32_t vcu_type() { return vcu_type_; }
  const std::string &log_path() { return log_path_; }
  const uint32_t radar_number() { return radar_number_; }
  const uint32_t sonar_channel_index() { return sonar_channel_index_; }
  const uint32_t sonar_channel_num() { return sonar_channel_num_; }

 private:
  uint32_t vcu_type_;
  std::string log_path_;
  uint32_t radar_number_;
  uint32_t sonar_channel_index_;
  uint32_t sonar_channel_num_;
};

}  // namespace chassis
}  // namespace PIAUTO