/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef CAN_OBJ_H
#define CAN_OBJ_H

#include <pthread.h>
#include <cmath>
#include <mutex>
#include "can_node/radar_77/radar_77.h"
#include "can_node/ultrasonic_radar/ultrasonic_radar.h"
#include "can_node/vehicle_control_unit/VCU.h"
#include "can_transmitter/can_transmitter.h"
#include "chassis_config.h"
#include "vehicle/command_ctrl.h"

namespace PIAUTO {
namespace chassis {

/**
 * @class CanObj.
 * @brief Singleton class which initialize the whole can system and provide
 * adaptor function. You can get CanNode object such as VCU or other kinds of
 * sensors via its interfaces.
 */
class CanObj {
 public:

  /**
   * @brief Initialize chassis configuration information via yaml file.
   * @param node Yaml file containing the chassis configuration information.
   */
  static void InitFromYaml(const YAML::Node &node) {
    config_ = std::make_unique<ChassisConfig>();
    config_->ParseFromYaml(node);
  }

  /**
   * @brief Initialize chassis configuration information via chassis_type parameter.
   * @param chassis_type Chassis type id,default is POD5.
   */
  static void InitFromPresets(const Chassis_Type &chassis_type) {
    config_ = std::make_unique<ChassisConfig>(chassis_type);
  }

  /**
   * @brief Static CanObj pointer getter.
   * @return Static CanObj pointer.
   */
  static CanObj *GetCanObj() {
    static std::once_flag oc;
    std::call_once(oc, [&] {
      try {
        CO.reset(new CanObj());
      } catch (std::exception &e) {
        LOG(ERROR) << "[chassis] " << e.what() << std::endl;
        exit(1);
      }
    });
    return CO.get();
  }

  /**
   * @brief Destructor.
   */
  ~CanObj();

  /**
   * @brief VCU data gets interface.
   * @return VCU data.
   */
  VCU &GetVCU() { return *vcu_; };

  /**
   * @brief Radar data gets interface.
   * @param index Radar number.
   * @return Radar data.
   */
  Radar_77 &GetRadar_77(int index) {
    if (index < 0 || index >= radar77s_.size()) {
      LOG(ERROR) << "[chassis] Failed to get Radar_77:bounds error";
    }
    return *(radar77s_[index]);
  }

  /**
   * @brief Get radar number.
   * @return Radar number.
   */
  const int GetRadar_77Number() { return radar77s_.size(); }

  /**
   * @brief Ultrasonic radar data gets interface.
   * @param index Ultrasonic radar number.
   * @return Ultrasonic radar data.
   */
  UltraSonicRadar &GetUSRadar(int index) {
    if (index < 0 || index >= ultrasonic_radars_.size()) {
      LOG(ERROR) << "[chassis] Failed to get sonar:bounds error";
    }
    return *(ultrasonic_radars_[index]);
  }

  /**
   * @brief Get ultrasonic radar number.
   * @return Ultrasonic radar number.
   */
  const unsigned GetUSRadarNumber() { return ultrasonic_radars_.size(); }

  /**
   * @brief Vehicle control commands interface,all commands must be sent through it.
   * @return Command execution result.
   */
  CommandCtrl &GetCommandCtrl() { return command_ctrl_; }

  /**
   * @brief Test all can nodes several times to judge if they are healthy when
   * ignite.
   * @return Test result.
   */
  bool SelfTest();

  /**
   * @brief Get chassis health state.
   * @return Chassis health state.
   */
  bool GetHealthState();

  /**
   * not used.
   */
  void Reset() { CO.reset(); }

 private:
  /**
   * @brief Singleton class constructor.
   * @param devtype Can device type.
   * @param index Can device index.
   * @param cannum Can bus index(more detail in can manual).
   */
  CanObj();

  static std::unique_ptr<CanObj> CO;

  CanTransmitter can_transmitter_;

  CommandCtrl command_ctrl_;

  std::shared_ptr<VCU> vcu_;

  std::atomic_uint vcu_health_count_;

  std::vector<std::shared_ptr<Radar_77>> radar77s_;

  std::vector<unsigned> radar77_health_index_;

  std::vector<std::shared_ptr<UltraSonicRadar>> ultrasonic_radars_;

  std::vector<unsigned> sonar_health_index_;

  /**
   * All connections between can nodes and Cantransmitter are stored
   * to disconnect them correctly when exit.
   */
  std::vector<boost::signals2::connection> connections;

  /// heart beat thread relatives
  std::unique_ptr<std::thread> heart_beat_thread_;
  void *HeartBeatThreadFunc();
  bool is_running_ = true;

  /// config
  static std::unique_ptr<ChassisConfig> config_;

  /**
   * @brief Register VCU type.
   * @param vcu_type Vcu type id,default is 3.
   * @param ct A transmitter used to initialize Can device.
   * @return VCU object pointer.
   */
  VCU *CreateVCU(int vcu_type, CanTransmitter *ct);
};
}  // namespace chassis
}  // namespace PIAUTO

#endif  // CAN_OBJ_H
