/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef CANTEST_COMMANDCTRL_H
#define CANTEST_COMMANDCTRL_H

#include <deque>
#include "can_node/vehicle_control_unit/VCU.h"


namespace PIAUTO {
    namespace passive_safety{
        class PassiveSafety;
    }

    namespace chassis {

        /// LOCK_FLAG enum.
        enum LOCK_FLAG {
            /// Forward lock flag.
            FORWARD = 0x00000080,
            /// Back lock flag.
            BACK = 0x00008000,
            /// Left lock flag.
            LEFT = 0x00800000,
            /// Right lock flag.
            RIGHT = 0x80000000
        };

        /// CommandType enum.
        enum CommandType {
            /// start car command, outmoded command.
            Ignite,
            /// Shutdown car command, outmoded command.
            ShutDown,
            /// Set Gear state, such as 'D','R','N'.
            SetGears,
            /// Set Throttle value, range from 0 to 100. Recommended use SetSpeed interface.
            SetThrottle,
            /// Set speed value, positive value move forward and negative value move back, the unit is m/s.
            SetSpeed,
            /// Set brake value, range from 0 to 100. Generally, if driving at a speed of 3m/s, the general
            /// obstacle avoidance requirement is recommended to be set to 70, which can take into account
            /// both safety and braking comfort. If emergency braking may be set to 100.
            SetBrake,
            /// Set Parking state, 1 for activation, 0 for inactivation.
            SetParkingState,
            /// Set Wheel angle value, range from -25 to +25 degree.
            SetWheelAngle,
            /// Set Drive mode. 0 for auto, 1 for manual, default is manual mode.
            SetDriveMode,
            /// Set angular velocity command, outmoded command.
            SetAngularVelocity,
            /// Set turn left angle value, range from 0 to +25 degree.
            TurnLeft,
            /// Set turn right angle value, range from 0 to +25 degree.
            TurnRight,
            /// Turn on drive light command, outmoded command.
            TurnOnDriveLight,
            /// Turn off drive light command, outmoded command.
            TurnOffDriveLight,
            /// Turn on low beam light command.
            TurnOnLowBeamLight,
            /// Turn off low beam light command.
            TurnOffLowBeamLight,
            /// Turn on high beam light command.
            TurnOnHighBeamLight,
            /// Turn off high beam light command.
            TurnOffHighBeamLight,
            /// Turn on left signal command.
            TurnOnLeftSignal,
            /// Turn off left signal command.
            TurnOffLeftSignal,
            /// Turn on right signal command.
            TurnOnRightSignal,
            /// Turn off right signal command.
            TurnOffRightSignal,
            /// Turn on horn command.
            TurnOnHorn,
            /// Turn off horn command.
            TurnOffHorn,
            /// Turn on flash light command.
            TurnOnFlashLight,
            /// Turn off flash light command.
            TurnOffFlashLight,
            /// Turn on brake light command.
            TurnOnBrakeLight,
            /// Turn off brake light command.
            TurnOffBrakeLight,
            /// Release manual intervention emergency state command.
            ReleaseEmergencyState,
        };

        /**
         * @class CommandCtrl
         * @brief Commands control module, all commands must be sent through it.
         * It will lock some commands when emergency occur.
         */
        class CommandCtrl {
        public:
            friend class passive_safety::PassiveSafety;
            /**
             * @brief Constructor.
             */
            CommandCtrl();

            /**
             * @brief Destructor.
             */
            ~CommandCtrl();

            /**
             * @brief Initialization vcu can node.
             * @param _vcu Commands are sent through vcu interfaces.
             */
            void Init(VCU *_vcu) {
                vcu = _vcu;
            }

            /**
             * @brief Read error code when command fails to execute.
             * @return Error code.
             */
            const ErrCode ReadErrCode();

            /**
             * @brief Read lock flag when started passive safety function, outmoded interface.
             * @return Lock flag.
             */
            const unsigned ReadLockFlag();

            /**
             * @brief Execute command interfaces
             * @param val Value needed for some commands such as "SetSpeed", and 0 is required if a command
             * does't need value,such as "TurnOnHorn".
             * @return Command execution result.
             */
            bool ExcCommand(const CommandType &, const int &val);

            bool ExcCommand(const CommandType &, const float &val);

            bool ExcCommand(const CommandType &, const char &val);

        private:

            /**
             * @brief Update lock status
             * @param sign Lock status value(0 for unlock status)
             */
            void UpdateLockFlag(unsigned sign);

            /**
             * @brief Used to set a fixed speed.
             * @return Command execution result.
             */
//            bool SetSpeed(const float);

            /**
             * @brief lockFlag, 7th bit for forward lock, 15th bit for backward lock,
             * 23ird bit for leftward lock, 31st bit for rightward lock
             */

            std::shared_timed_mutex lock_flag_mt;

            unsigned lockFlag;

            VCU *vcu;

            ErrCode errCode;
        };
    }
}
#endif //CANTEST_COMMANDCTRL_H
