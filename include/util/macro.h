/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef ADU_CLIENT_MACRO_H
#define ADU_CLIENT_MACRO_H

#define DISALLOW_COPY_AND_ASSIGN(classname) \
 private:                                   \
  classname(const classname &);             \
  classname &operator=(const classname &);

#define DISALLOW_IMPLICIT_CONSTRUCTORS(classname) \
 private:                                         \
  classname();                                    \
  DISALLOW_COPY_AND_ASSIGN(classname);

#endif //ADU_CLIENT_MACRO_H
