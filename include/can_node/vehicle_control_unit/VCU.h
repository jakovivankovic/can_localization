/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef ADU_CLIENT_VCU_H
#define ADU_CLIENT_VCU_H

#include "can_node/can_node.h"
#include "can_transmitter/frame_controller.h"
#include "PITime.h"

/**
 * @brief PIAUTO
 */
namespace PIAUTO {
    /**
     * @brief chassis
     */
    namespace chassis {

        /// Error type returned when commands executed.
        enum class ErrCode {
            ///  Command execute successfully.
            OK =                 1,
            /// CanBus error.
            CAN_ERR =            0,
            /// Value out of range.
            VAL_BOUND_ERR =     -1,
            /// Execute commands too frequently.
            TIME_BOUND_ERR =    -2,
            /// Try to accelerate in lock status.
            LOCK_ERR =          -3,
            /// Try to accelerate when electronic brake is activated.
            BRAKE_ERR =         -4,
            /// Command type and value type are not consistent.
            TYPE_ERR =          -5,
            /// Command not exist.
            NONE =              -6
        };


        /// Drive mode enum.
        enum class DriveMode {
            /// Auto mode.
            AUTO =              0,
            /// Manual mode.
            MANUAL =            1,
            /// Manual intervention when some emergency happened in auto mode,
            /// then needs to invoke ReleaseEmergencyState() to switch to recover.
            INTERVENE =         2,
            /// Reserved.
            RESERVED =          -1
        };

        /**
         * @class VCU
         * @brief VCU class derived from CanNode, a kind of can node which store
         * VCU data and provide corresponding interfaces to set and get them.
         */
        class VCU : public CanNode {
        public:

            /**
             * @brief Constructor.
             */
            VCU(CanTransmitter* can_transmitter):can_transmitter_(can_transmitter){};

            /**
             * @brief Destructor.
             */
            virtual ~VCU() {};

            /// CommandCtrl module execute command by using VCU interfaces
            friend class CommandCtrl;

            /**
             * @brief Gears mode gets interface.
             * @return Gears mode.
             */
            virtual const char GetGears()=0;

            /**
             * @brief Throttle value gets interface.
             * @return Throttle value(0~100).
             */
            virtual const int GetThrottle()=0;

            /**
             * @brief Speed value gets interface.
             * @return Speed value(unit:m/s).
             */
            virtual const float GetSpeed()=0;

            /**
             * @brief Brake value gets interface.
             * @return Brake value(0~100).
             */
            virtual const int GetBrake()=0;

            /**
             * @brief Parking state gets interface.
             * @return Parking state.
             */
            virtual bool GetParkingState()=0;

            /**
             * @brief Current wheel angle gets interface.
             * @return Current wheel angle value,positive value means turn left,negative value means turn right(unit:degree).
             */
            virtual const float GetWheelAngle()=0;

            /**
             * @brief Angular velocity gets interface.
             * @return Angular velocity value(unit:deg/s).
             */
            virtual const float GetAngularVelocity();

            /**
             * @brief Battery power gets interface.
             * @return Battery percentage(%).
             */
            virtual const float GetBatteryPower();

            /**
             * @brief Battery voltage gets interface.
             * @return Battery voltage(unit:V).
             */
            virtual const float GetBatteryVoltage();

            /**
             * @brief Drive mode gets interface.
             * @return Drive mode,include 'AUTO', 'MANUAL','INTERVENE' and 'RESERVED'.
             */
            virtual const DriveMode GetDriveMode();

            /**
             * @brief Driving light state gets interface.
             * @return Driving light state.
             */
            virtual bool GetDrivingLight();

            /**
             * @brief Low beam light state gets interface.
             * @return Low beam light state.
             */
            virtual bool GetLowBeamLight();

            /**
             * @brief High beam light state gets interface.
             * @return High beam light state.
             */
            virtual bool GetHighBeamLight();

            /**
             * @brief Left signal light state gets interface.
             * @return Left signal light state.
             */
            virtual bool GetLeftSignal();

            /**
             * @brief Right signal light state gets interface.
             * @return Right signal light state.
             */
            virtual bool GetRightSignal();

            /**
             * @brief Brake light state gets interface.
             * @return Brake light state.
             */
            virtual bool GetBrakeLight();

            /**
             * @brief Reversing light state gets interface.
             * @return Reversing light state.
             */
            virtual bool GetReversingLight();

            /**
             * @brief Horn state gets interface.
             * @return Horn state.
             */
            virtual bool GetHorn();

            /**
             * @brief Flash light state gets interface.
             * @return Flash light state.
             */
            virtual bool GetFlashLight();

            /**
             * @brief Generate CAN heart beat frame.
             * @return VCI_CAN_OBJ struct data.
             */
            virtual VCI_CAN_OBJ GenerateHeartBeatFrame();

        protected:

            virtual ErrCode Ignite();

            virtual ErrCode ShutDown();

            virtual ErrCode SetGears(char val)=0;

            virtual ErrCode SetThrottle(int val)=0;

            virtual ErrCode SetSpeed(float val)=0;

            virtual ErrCode SetBrake(unsigned val)=0;

            virtual ErrCode SetParkingState(bool val)=0;

            virtual ErrCode SetWheelAngle(float val)=0;

            virtual ErrCode SetAngularVelocity(float val);

            virtual ErrCode SetDriveMode(const DriveMode);

            virtual ErrCode TurnLeft(float ang);

            virtual ErrCode TurnRight(float ang);


            virtual ErrCode TurnOnDrivingLight();

            virtual ErrCode TurnOffDrivingLight();

            virtual ErrCode TurnOnLowBeamLight();

            virtual ErrCode TurnOffLowBeamLight();

            virtual ErrCode TurnOnHighBeamLight();

            virtual ErrCode TurnOffHighBeamLight();

            virtual ErrCode TurnOnLeftSignal();

            virtual ErrCode TurnOffLeftSignal();

            virtual ErrCode TurnOnRightSignal();

            virtual ErrCode TurnOffRightSignal();

            virtual ErrCode TurnOnBrakeLight();

            virtual ErrCode TurnOffBrakeLight();

            virtual ErrCode TurnOnReversingLight();

            virtual ErrCode TurnOffReversingLight();

            virtual ErrCode TurnOnHorn();

            virtual ErrCode TurnOffHorn();

            virtual ErrCode TurnOnFlashLight();

            virtual ErrCode TurnOffFlashLight();

            virtual ErrCode ReleaseEmergencyState();


            CanTransmitter *can_transmitter_;
            std::timed_mutex frames_mt_;
            std::vector<FrameController*> frames_;
            bool is_running_ = false;
            std::unique_ptr<std::thread> send_thread_;
            time::Timer throttle_protect_timer_;
            void *SendThreadFunc();
            void StartSendThread();
            void StopSendThread();
        };
    }
}


#endif //ADU_CLIENT_VCU_H
