/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef CHASSIS_ULTRASONICRADAR_H
#define CHASSIS_ULTRASONICRADAR_H

#include "can_node/can_node.h"

namespace PIAUTO {
    namespace chassis {

        /// Ultrasonic radar buffer
        struct USRadarAttributes;

        /// Ultrasonic radar data struct.
        struct SonarData {
            /// Distance from obstacle,unit: millimeter.
            unsigned short Range;
            /// Ultrasonic frame message timestamp,unit: milliseconds.
            double timestamp;
            /// Local system time,unit: milliseconds.
            uint64_t linux_time;
        };

        std::ostream &operator<<(std::ostream &os, const SonarData &sonar_obj);

        /**
         * @class UltraSonicRadar
         * @brief Ultrasonic radar class derived from CanNode, a kind of can node which store
         *          ultrasonic radar data and provide corresponding interfaces to get them.
         */
        class UltraSonicRadar : public CanNode {
        public:
            /**
             * @brief Constructor and destructor
             * @param id Sonar ID
             * @param c Cantransmitter used to send some config info(not used)
             * @param channel_num Default value is 1ECU_2channels
             */
            UltraSonicRadar(int id, CanTransmitter *c, int channel_num = 2);

            ~UltraSonicRadar() override;

            typedef boost::signals2::signal<void(const int, const SonarData &)> sonar_signal_t;

            /**
             * @brief Ultrasonic radar data subscribe interface, subscribers are informed when data is updated.
             * @param subscriber Call-back function.
             * @return Connection
             */
            boost::signals2::connection SubscribeToSonar(const sonar_signal_t::slot_type &subscriber);

            /**
             * @brief Update ultrasonic radar data when received CAN frame message.
             * @return Update result.
             */
            bool UpdateAttributes(VCI_CAN_OBJ &) override;

            /**
             * @brief Verify ultrasonic radar CAN frame update time.
             * @return Verify result.
             */
            bool VerifyFrameTimer() override;

        private:
            int sonar_ID_;
            int channel_num_;
            CanTransmitter *ct;
            USRadarAttributes *attri;
            sonar_signal_t m_sonar_sig;
        };

    }  // namespace chassis
}  // namespace PIAUTO
#endif //CHASSIS_ULTRASONICRADAR_H
