/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef CANTEST_RADAR77_H
#define CANTEST_RADAR77_H

#include <can_node/can_node.h>

namespace PIAUTO {
    namespace chassis {

        /// radar77 buffer
        struct Radar77Attributes;

        /// radar77 data struct.
        struct ObjectInfo_77 {
            /// Track target id.
            int index;
            /// Track target distance,unit: m.
            float Range;
            /// Track target radial velocity,speed away from radar is negative, speed near radar is positive,unit: m/s.
            float RadialVelocity;
            /// Track target radial Acceleration,unit: m/s2.
            float RadialAcc;
            /// Track target azimuth,the right angle of the radar is negative and the left angle is positive,unit: degree.
            float Azimuth;
            /// Track target returned power,unit: dBm.
            float Power;
            /// Radar77 frame message timestamp,unit: milliseconds.
            double timestamp;
            /// Local system time,unit: milliseconds.
            uint64_t linux_time;
        };

        typedef std::vector<ObjectInfo_77> Radar77Data;

        std::ostream &operator<<(std::ostream &os, const ObjectInfo_77 &obj);

        /**
         * @class Radar_77
         * @brief Radar77 class derived from CanNode, a kind of can node which store
         *          radar77 data and provide corresponding interfaces to get them.
         */
        class Radar_77 : public CanNode {
        public:
            /**
             * @brief Constructor and destructor
             * @param id Radar ID
             * @param c Cantransmitter used to send some config info(not used)
             */
            Radar_77(int id, CanTransmitter *c);

            ~Radar_77() override;

            typedef boost::signals2::signal<void(const int, const Radar77Data &)> radar_signal_t;

            /**
             * @brief Radar data subscribe interface, subscribers are informed when data is updated.
             * @param subscriber Call-back function.
             * @return Connection
             */
            boost::signals2::connection SubscribeToRadar(const radar_signal_t::slot_type &subscriber);

            /**
             * @brief Update radar_77 data when received radar CAN frame message.
             * @return Update result.
             */
            bool UpdateAttributes(VCI_CAN_OBJ &) override;

            /**
             * @brief Verify radar_77 CAN frame update time.
             * @return Verify result.
             */
            bool VerifyFrameTimer() override;

        private:
            int ID;
            CanTransmitter *ct;
            Radar77Attributes *attri;
            int obj_index=0;
            Radar77Data data_once_;
            radar_signal_t m_radar_sig;
        };
    }
}


#endif //CANTEST_RADAR77_H
