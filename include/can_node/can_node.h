/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef CANTEST_CANNODE_H
#define CANTEST_CANNODE_H

#include <boost/signals2/signal.hpp>
#include <shared_mutex>
#include <vector>
#include "PITime.h"
#include "can_transmitter/can_transmitter.h"

namespace PIAUTO {
    namespace chassis {

        /**
         * @class CanNode
         * @brief Bass class of all can node class
         */
        class CanNode {
        public:
            /**
             * @brief Constructor
             */
            CanNode() = default;
            /**
             * @brief Destructor
             */
            virtual ~CanNode() = default;

            /**
             * @brief Analyze and extract frame data to can node object's buffer.
             * @param frame Frame to be analyzed.
             * @return True If frame is belong to this kind of can node.
             */
            virtual bool UpdateAttributes(VCI_CAN_OBJ &frame)=0;

            /**
             * @brief Verify frames' interval to judge if the node is healthy.
             * @return True If time is less than limited time.
             */
            virtual bool VerifyFrameTimer()=0;


        protected:
            std::shared_timed_mutex mt_;
        };

    }
}
#endif //CANTEST_CANNODE_H
