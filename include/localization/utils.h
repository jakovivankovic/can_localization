/** PerceptIn license **/

#pragma once

#include <cstdint>
#include <ctime>
#include <regex>
namespace PIAUTO {
namespace localization {

namespace util {
uint64_t GetCurrentTimeMilliSec();

double GetGaussBias(double avr = 0, double rms = 1);

bool IsIntegerType(const std::string &str_);
};  // namespace util
};  // namespace localization
}  // namespace PIAUTO