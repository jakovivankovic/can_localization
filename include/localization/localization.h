/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/
#pragma once

#include "localization/dragonflyvio_client.h"
#include "localization/vio_com.h"

namespace PIAUTO {

/**
 * @brief localization namespace
 */
namespace localization {

enum PositionType {
  /// positon is fusion mode
  POSITION_TYPE_FUSION = -1,
  /// positon is float mode
  POSITION_TYPE_FLOAT = 2,
  /// positon is fixed mode
  POSITION_TYPE_FIXED = 4
};

enum HeadingType {
  /// heading is fusion mode
  HEADING_TYPE_FUSION = -1,
  /// heading is float mode
  HEADING_TYPE_FLOAT = 2,
  /// heading is fixed mode
  HEADING_TYPE_FIXED = 4
};

enum LocalizationStatus {
  /// GPS_ONLY mode means pure GPS no fusion
  GPS_ONLY = 0,
  /// postion is GPS mode, heading is GPS mode
  POSEGPS_HEADINGGPS = 1,
  /// postion is GPS mode, heading is fusion mode
  POSEGPS_HEADINGFUSION = 2,
  /// postion is FUSION mode, heading is GPS mode
  POSEFUSION_HEADINGGPS = 3,
  /// postion is FUSION mode, heading is FUSION mode
  POSEFUSION_HEADINGFUSION = 4,
  /// error
  ERROR = -1
};

enum ErrorType {
  // sensor device connect error
  /// Dragonfly sensor disconnect
  DF_DISCONNECT = -1,
  /// Dragonfly sensor no response
  DF_NO_RESPONSE = -2,
  /// Dragonfly sensor error occurred
  DF_ERROR_OCCURRED = -3,
  /// Dragonfly sensor time unsync
  DF_TIME_UNSYNC = -4,

  // sensor open error
  /// image open error
  IMAGE_OPEN_FAILED = -11,
  /// IMU open error
  IMU_OPEN_FAILED = -12,
  /// GPS open error
  GPS_OPEN_FAILED = -13,

  // sensor data error
  /// localization module not received image
  IMAGE_NOT_RECEIVED = -21,
  /// localization module not valid image
  IMAGE_NOT_VALID = -22,
  /// localization module not received IMU
  IMU_NOT_RECEIVED = -23,
  /// localization module not valid IMU
  IMU_NOT_VALID = -24,
  /// localization module not received GPS
  GPS_NOT_RECEIVED = -25,
  /// localization module not valid GPS
  GPS_NOT_VALID = -26,

  // init error
  /// gravity init error
  GRAVITY_INIT_FAILED = -31,
  /// relative pose between GPS coordinate and Dragonfly coordinate init error
  GPS_DRAGONFLY_POSE_INIT_FAILED = -32,

  // client error
  /// localization client init failed
  CLIENT_INIT_FAILED = -41,
  /// received msg repeat stamp
  MSG_REPEAT_STAMP = -42,
  /// received msg timeout or disorder
  MSG_TIMEOUT_OR_DISORDER = -43,
  /// localization client not received message
  CLIENT_NOT_RECEIVED_MSG = -44,

  // other
  /// unknow error
  UNKNOW_ERROR = -51,
};

struct LocalizationData {
  // Timestamp
  /// machine timestamp in seconds
  double local_stamp;
  /// GPS global timestamp in seconds
  double gps_stamp;

  // Position
  /// UTM x (meter)
  double utm_x;
  /// UTM y (meter)
  double utm_y;
  /// UTM x variance (meter)
  double utm_x_variance;
  /// UTM y variance (meter)
  double utm_y_variance;
  /// UTM time zone
  int utm_zone;
  /// position type
  PositionType position_type;

  // Heading
  /// heading value
  double heading;
  /// heading variance
  double heading_variance;
  /// heading type
  HeadingType heading_type;

  // System status
  /// localization status
  LocalizationStatus localization_status;
  /// error code
  ErrorType error_code;

  /// print function
  friend std::ostream &operator<<(std::ostream &os, LocalizationData &data);
};

class VIGLocalization  {
 public:
  /**
   * @brief constructor function
   * @param ip_str the address of localiztion server, e.g. "tcp://127.0.0.1"
   */
  VIGLocalization(const std::string &ip_str);

  /**
   * @brief destructor function
   */
  ~VIGLocalization();

  /**
   * @brief get localization data
   * @param data the localization data struct
   * @return get data success or failed
   */
  bool GetLocalization(LocalizationData &data);

  /**
   * @brief  1) initialize localization client 2) wait the localization server initialize successful
   * @return init success or failed
   */
  bool Init();

  /**
   * @brief finish the usage of localizaion API
   */
  void Finish();

  /**
   * @brief debug function
   */
  void PrintRawData();

 private:
  void Update(const PILocalizationVioMsg &pos);

  std::string ip_str;
  PILocalizationVioMsg pos;
  std::shared_ptr<PIAUTO::dragonflyvio::DragonflyvioClient> client = nullptr;

  double last_check_local_stamp = -1;  // ms
  double check_interval = 500;         // ms
  double last_check_pos_stamp;
  bool had_wrong_value = false;
  bool server_init_succ = false;
  std::chrono::high_resolution_clock::time_point print_time_point;
  int print_time_interval = 1; // seconds
};

}  // namespace localiztion
}  // namespace PIAUTO
