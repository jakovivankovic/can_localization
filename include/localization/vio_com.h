/** PerceptIn license **/

#ifndef PI_VIO_COM_H
#define PI_VIO_COM_H

enum GPSFusionMode
{
  // mode
  GPSFusionMode_GPS = 0,
  GPSFusionMode_FUSION_POSEGPS_HEADINGGPS = 1,
  GPSFusionMode_FUSION_POSEGPS_HEADINGVIO = 2,
  GPSFusionMode_FUSION_POSEVIO_HEADINGGPS = 3,
  GPSFusionMode_FUSION_POSEVIO_HEADINGVIO = 4,

  // sensor error
  GPSFusionMode_CONNECTION_ERROR = -10,
  GPSFusionMode_UNRESPONSE_ERROR = -11,
  GPSFusionMode_IMAGE_ERROR = -12,
  GPSFusionMode_IMU_ERROR = -13,
  GPSFusionMode_ERROR_HAPPED = -14,

  GPSFusionMode_GPSONLY_GPSTIME_NAN = -15,
  GPSFusionMode_GPSONLY_POS_NOT_FIXED = -16,
  GPSFusionMode_GPSONLY_POS_NAN = -17,
  GPSFusionMode_GPSONLY_HEADING_NOT_FIXED = -18,
  GPSFusionMode_GPSONLY_HEADING_NAN = -19,
  GPSFusionMode_TIME_UNSYNC = -20,

  // init error
  GPSFusionMode_NO_FIRST_VALID_IMU = -30,
  GPSFusionMode_NO_FIRST_VALID_IMAGE = -31,
  GPSFusionMode_NO_FIRST_VALID_GPS = -32,
  GPSFusionMode_INIT_GRAVITY_FAILED = -33,
  GPSFusionMode_INIT_GPS_FAILED = -34,

  // other
  GPSFusionMode_UNKNOW_ERROR = -41,
  GPSFusionMode_CLIENT_UNINIT = -42,
  GPSFusionMode_CLIENT_REPEAT_STAMP = -43,
  GPSFusionMode_CLIENT_NOT_RECEIVED_MSG = -44,

};

const std::unordered_map<int, std::string> Mode2Name {
    {GPSFusionMode_GPS, "GPS_ONLY"},
    {GPSFusionMode_FUSION_POSEGPS_HEADINGGPS, "FUSION_POSEGPS_HEADINGGPS"},
    {GPSFusionMode_FUSION_POSEGPS_HEADINGVIO, "FUSION_POSEGPS_HEADINGVIO"},
    {GPSFusionMode_FUSION_POSEVIO_HEADINGGPS, "FUSION_POSEVIO_HEADINGGPS"},
    {GPSFusionMode_FUSION_POSEVIO_HEADINGVIO, "FUSION_POSEVIO_HEADINGVIO"},

    {GPSFusionMode_CONNECTION_ERROR, "DF_DISCONNECT"},
    {GPSFusionMode_UNRESPONSE_ERROR, "DF_NO_RESPONSE"},
    {GPSFusionMode_ERROR_HAPPED, "DF_ERROR_OCCURRED"},
    {GPSFusionMode_TIME_UNSYNC, "DF_TIME_UNSYNC"},

    {GPSFusionMode_IMAGE_ERROR, "IMAGE_OPEN_FAILED"},
    {GPSFusionMode_IMU_ERROR, "IMU_OPEN_FAILED"},

    {GPSFusionMode_GPSONLY_GPSTIME_NAN, "GPSONLY_GPSTIME_NAN"},
    {GPSFusionMode_GPSONLY_POS_NOT_FIXED, "GPSONLY_POS_NOT_FIXED"},
    {GPSFusionMode_GPSONLY_POS_NAN, "GPSONLY_POS_NAN"},
    {GPSFusionMode_GPSONLY_HEADING_NOT_FIXED, "GPSONLY_HEADING_NOT_FIXED"},
    {GPSFusionMode_GPSONLY_HEADING_NAN, "GPSONLY_HEADING_NAN"},

    {GPSFusionMode_NO_FIRST_VALID_IMAGE, "IMAGE_NOT_VALID"},
    {GPSFusionMode_NO_FIRST_VALID_IMU, "IMU_NOT_RECEIVED"},
    {GPSFusionMode_NO_FIRST_VALID_GPS, "GPS_NOT_VALID"},

    {GPSFusionMode_INIT_GRAVITY_FAILED, "GRAVITY_INIT_FAILED"},
    {GPSFusionMode_INIT_GPS_FAILED, "GPS_DRAGONFLY_POSE_INIT_FAILED"},

    {GPSFusionMode_CLIENT_UNINIT, "CLIENT_INIT_FAILED"},
    {GPSFusionMode_CLIENT_REPEAT_STAMP, "MSG_REPEAT_STAMP"},
    {GPSFusionMode_CLIENT_NOT_RECEIVED_MSG, "CLIENT_NOT_RECEIVED_MSG"},

    {GPSFusionMode_UNKNOW_ERROR, "UNKNOW_ERROR"},
};

struct PILocalizationVioMsg
{
  // local timestamp, in the unit of second
  double stamp;

  // gps global timestamp
  double gstamp;

  // position of DragonFly with respect to the world origin
  double tx;
  double ty;
  double tz;

  // orientation of DragonFly (in terms of unit quaternion) with respect to the world origin
  double qx;
  double qy;
  double qz;
  double qw;

  // UTM coordinates, valid when gnss_fusion is enabled
  double utm_x;
  double utm_y;
  int utm_zone;
  double gheading;

  // gnss fusion pos_status
  GPSFusionMode fusion_mode;  // 0: gps bypass; 1: vio fusion
  int pos_status;
  int heading_status;
  double accuracy; // 0.0 under gps bypass mode; accumulated with time under vio fusion mode.
};

#endif
