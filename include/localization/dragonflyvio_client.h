#ifndef DRAGONFLY_VIO_CLIENT_H
#define DRAGONFLY_VIO_CLIENT_H


#include <functional>
#include <thread>
#include <string>
#include <unordered_map>

#include <boost/signals2/signal.hpp>
#include <nanomsg/nn.h>
#include <nanomsg/pubsub.h>

#include "msg_utils/pi_msg_com.h"
#include "msg_utils/pi_msg_adaptor.h"
#include "localization/vio_com.h"

namespace PIAUTO
{
namespace dragonflyvio
{

struct VIOClientStatus{
  GPSFusionMode mode;
  std::string mode_name;
};

class DragonflyvioClient
{
public:
    // construction

    // deconstruction
    ~DragonflyvioClient();

    bool init(const std::string& vio_server_url);

    // callback function prototype
    using dragonflyvio_client_callback_t = void(const PILocalizationVioMsg& pos);

    // Register one callback which will be excecuted when new vio pos received.
    void addCallback(std::function<dragonflyvio_client_callback_t> callback);

    VIOClientStatus getStatus();

    // Start to listen to dragonfly vio server.
    // Please note that it's important to call start manually after construction and initialization,
    // Otherwise you will receive none pos update.
    void start();

    // Stop listening to dragonfly vio server.
    // It will be called at deconstruction, so their's no need for extra stop.
    void stop();

    GPSFusionMode status = GPSFusionMode_CLIENT_UNINIT;

private:
    double last_stamp = -1;
    std::chrono::high_resolution_clock::time_point last_new_data_time_point;
    double repreat_time;
    int repeat_count;

    std::shared_ptr<PIAUTO::msg::PIMsgAdaptor> m_msg_adp;
    int m_sub_handler;

    std::function<dragonflyvio_client_callback_t> callback_;
};
}

}

#endif // DRAGONFLY_VIO_CLIENT_H
