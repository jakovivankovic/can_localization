/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef ADU_CLIENT_CAN_TRANSMITTER_CONFIG_H
#define ADU_CLIENT_CAN_TRANSMITTER_CONFIG_H

#include <yaml-cpp/yaml.h>

namespace PIAUTO {
namespace chassis {
class CanTransmitterConfig {
 public:
  CanTransmitterConfig() = default;
  void ParseFromYaml(const YAML::Node& node) {
    devtype_ = node["device_type"].as<uint32_t>();
    index_ = node["index"].as<uint32_t>();
    code_ = node["code"].as<uint32_t>();
    mask_ = node["mask"].as<uint32_t>();
    baud_rate_ = node["baud_rate"].as<uint32_t>();
    filter_type_ = node["filter_type"].as<uint32_t>();
    mode_ = node["mode"].as<uint32_t>();

    enable_frame_record_ = node["enable_frame_record"].as<bool>();
    frame_record_path_ = node["frame_record_path"].as<std::string>();
    auto temp_filter = node["frame_record_id_filter"].as<std::vector<uint32_t>>();
    frame_record_id_filter_.insert(temp_filter.begin(), temp_filter.end());

    enable_display_send_frame_ = node["enable_display_send_frame"].as<bool>();
    temp_filter = node["send_frame_id_filter"].as<std::vector<uint32_t>>();
    send_frame_id_filter_.insert(temp_filter.begin(), temp_filter.end());
    enable_frame_monitor_ =  node["enable_frame_monitor"].as<bool>();
  }

  const uint32_t devtype() { return devtype_; }
  const uint32_t index() { return index_; }
  const uint32_t code() { return code_; }
  const uint32_t mask() { return mask_; }
  const uint32_t baud_rate() { return baud_rate_; }
  const uint32_t filter_type() { return filter_type_; }
  const uint32_t mode() { return mode_; }
  bool& doubel_can() { return double_can_; }
  const bool enable_frame_record() { return enable_frame_record_; }
  const bool enable_display_send_frame() { return enable_display_send_frame_; }
  const std::string& frame_record_path() { return frame_record_path_; }
  const std::unordered_set<unsigned>& frame_record_id_filter() { return frame_record_id_filter_; }
  const std::unordered_set<unsigned>& send_frame_id_filter() { return send_frame_id_filter_; }
  const bool enable_frame_monitor() {return enable_frame_monitor_; };

 private:
  uint32_t devtype_ = 4;
  uint32_t index_ = 0;
  uint32_t code_ = 0;
  uint32_t mask_ = 0xffffffff;
  uint32_t baud_rate_ = 500;
  uint32_t filter_type_ = 0;
  uint32_t mode_ = 0;

  bool double_can_ = true;
  bool enable_frame_record_ = false;
  std::string frame_record_path_ = "./frame_record";
  std::unordered_set<unsigned> frame_record_id_filter_;
    bool enable_display_send_frame_ = false;
    std::unordered_set<unsigned> send_frame_id_filter_;
    bool enable_frame_monitor_ = false;
};
}  // namespace chassis
}
#endif  // ADU_CLIENT_CAN_TRANSMITTER_CONFIG_H
