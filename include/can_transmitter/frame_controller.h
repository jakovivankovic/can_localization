/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#ifndef ADU_CLIENT_FRAME_CONTROLLER_H
#define ADU_CLIENT_FRAME_CONTROLLER_H

#include <functional>
#include "PITime.h"
#include "controlcan.h"

namespace PIAUTO {
namespace chassis {

class FrameController {
public:
  typedef std::function<void(VCI_CAN_OBJ &)> CheckSumFuncType;
  FrameController(const uint32_t &frame_ID, const uint32_t &data_len,
                  const uint32_t &interval, CheckSumFuncType func = nullptr);
  bool NeedToSend();
  void Reset();
  void SetByteData(int32_t val, int32_t begin, const int32_t length);
  void SetByteHigh4bit(int32_t val, int32_t byte_index);
  void SetByteLow4bit(int32_t val, int32_t byte_index);
  void SetBit1(int32_t byte_index, int32_t bit_index);
  void SetBit0(int32_t byte_index, int32_t bit_index);
  VCI_CAN_OBJ &GetFrame() { return frame_; }
  void GenerateChecksum() {
    if (generate_checksum_ != nullptr) {
      generate_checksum_(frame_);
    }
  }

private:
  CheckSumFuncType generate_checksum_;
  VCI_CAN_OBJ frame_;
  time::Timer timer_;
  bool updated_;
  const uint32_t interval_;
};

} // namespace chassis
} // namespace PIAUTO

#endif // ADU_CLIENT_FRAME_CONTROLLER_H
