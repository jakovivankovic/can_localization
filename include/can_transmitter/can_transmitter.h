/******************************************************************************
 * PerceptIn license.
 *****************************************************************************/

#pragma once

#include <glog/logging.h>
#include <atomic>
#include <boost/signals2/signal.hpp>
#include <fstream>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_set>
#include "PITime.h"
#include "can_transmitter/controlcan.h"
#include "can_transmitter_config.h"
#include "util/macro.h"

namespace PIAUTO {
namespace chassis {
/**
 * @brief Convert FrameData to string.
 * @param frame Frame to be converted.
 * @return String converted from the frame.
 */
std::string Frame2Str(VCI_CAN_OBJ &frame);

/**
 * @brief Record frameId and data to a file.
 * @param file File to store the data
 * @param frame Frame to be recorded.
 */
//
void RecordFrame(std::fstream &file, VCI_CAN_OBJ &frame);

/**
 * @brief Convert char(0-f) to int(0-15)
 * @param chr Char to be converted.
 * @param[out] cint Int converted from char.
 * @return True If chr is between 0-f or 0-F.
 */
bool CharToInt(const unsigned char chr, unsigned char *cint);

/**
 * @brief Convert hex string to char array:
 *        Convert "21" to 0x21(1 byte) ,"2121" to 0x2121(2 byte) ,"21 21" to 0x2121 if flag=1
 * @param str String to be converted.
 * @param[out] data Char array converted from string.
 * @param len Pair number in str("21" means a pair).
 * @param flag 1 means space exists between pairs.
 * @return True If chr is between 0-f or 0-F.
 */
bool StrToData(const unsigned char *str, unsigned char *data, const int len, const int flag);

/**
 * @brief Insert val into raw data from begin to begin+lenth index
 * @param raw Data to be changed.
 * @param val Value to be inserted into raw.
 * @param begin Begin index.
 * @param length Bytes' number the val occupied.
 */
void InsertDataOnBytes(BYTE *raw, const int val, const int begin, const int length);

/**
 * @brief Reverse high-Low Byte
 * @param val Value to be reversed.
 * @return result.
 */
USHORT ReverseHLValue(const USHORT val);

/**
 * @class CanTransmitter
 * @brief A transmitter used to initialize Can device, generate and send frame,
 * receive frames and distribute them to various Cannodes' buffers.
 */
class CanTransmitter {
 public:
  /**
   * @brief Constructor
   */
  CanTransmitter() = default;
  /**
   * @brief Destructor
   */
  ~CanTransmitter() = default;

  /**
   * @brief Update config from yaml
   * @param Input yaml node
   */
   void GetConfigFromYaml(const YAML::Node& node);


  /**
   * @brief Initialization of the can device.
   */
  void Init();

  /**
   * @brief Close the can device
   */
  void Close();

  /**
   * @brief Make up a frame with some string
   * @param ID Frame ID.
   * @param Data Frame data(8 bytes).
   * @param SendType 0 for normal, 1 for once, 2 for self-test, 3 for self-test once.
   * @param ExternFlag 1 for extended frame.
   * @param RomoteFlag 1 for remote frame.
   * @return Frame generated.
   */
  static VCI_CAN_OBJ GenerateFrame(const std::string &ID, const std::string &data, const BYTE send_type = 0,
                                   const BYTE extern_flag = 0, const BYTE remote_flag = 0);

  /**
   * @brief Generate checksum byte for a frame.
   * @param frame Frame to be handled.
   */
  static void GenerateChecksum(VCI_CAN_OBJ *frame);

  typedef boost::signals2::signal<void(VCI_CAN_OBJ &)> frame_event_t;

  /**
   * @brief Subscribe To frameEvent, subscribers are informed when a new frame arrived.
   * @param slot_type Call-back function.
   */
  boost::signals2::connection SubscribeToFrameEvent1(const frame_event_t::slot_type &);

  boost::signals2::connection SubscribeToFrameEvent2(const frame_event_t::slot_type &);

  /**
   * @brief Send frame via canbus.
   * @return True If no error occur.
   */
  bool SendData(VCI_CAN_OBJ *);

  /**
   * @brief Receive frames thread.
   */
  void *ReceiveData();

  void *FrameRateMonitor();

 private:
  std::thread *monitor_thread_;
  std::atomic<unsigned> send_frame_num_;
  std::atomic<unsigned> recv_frame_num_;
  unsigned frame_rate_ = 0;
  unsigned max_frame_rate_ = 0;
  unsigned max_send_frame_rate_ = 0;
  unsigned max_recv_frame_rate_ = 0;

 private:
  /// connect status
  bool is_running_;
  std::thread *recv_data_thread_;
  std::mutex mt_;

  std::fstream log_file_;
  frame_event_t frame_event_1_;
  frame_event_t frame_event_2_;

  CanTransmitterConfig config_;

  DISALLOW_COPY_AND_ASSIGN(CanTransmitter);
};
}  // namespace chassis
}