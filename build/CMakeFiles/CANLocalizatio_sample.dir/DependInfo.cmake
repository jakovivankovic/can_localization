# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/perceptin/Copy_Product-sdk-v1.0 (copy).0/PI_SDK/modules/CAN_Localization/src/sample.cpp" "/home/perceptin/Copy_Product-sdk-v1.0 (copy).0/PI_SDK/modules/CAN_Localization/build/CMakeFiles/CANLocalizatio_sample.dir/src/sample.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_ON_NONTEGRA"
  "CPU_ONLY"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../../../ThirdParty/Linux-X64/glog/include"
  "../../../ThirdParty/Linux-X64/gflags/include"
  "../../../ThirdParty/yaml-cpp/include"
  "../../../ThirdParty/Linux-X64/boost/include"
  "../../../ThirdParty/pi_time/include"
  "../../../ThirdParty/msg_utils/include"
  "../../../ThirdParty/Linux-X64/nanomsg/include"
  "../../../ThirdParty/protobuf/x64/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
