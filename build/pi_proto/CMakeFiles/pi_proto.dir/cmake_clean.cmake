file(REMOVE_RECURSE
  "geometry.pb.cc"
  "geometry.pb.h"
  "route.pb.cc"
  "route.pb.h"
  "header.pb.cc"
  "header.pb.h"
  "planning.pb.cc"
  "planning.pb.h"
  "chassis.pb.cc"
  "chassis.pb.h"
  "perception_obstacle.pb.cc"
  "perception_obstacle.pb.h"
  "decision.pb.cc"
  "decision.pb.h"
  "planning_internal.pb.cc"
  "planning_internal.pb.h"
  "localization.pb.cc"
  "localization.pb.h"
  "operational_data.pb.cc"
  "operational_data.pb.h"
  "CMakeFiles/pi_proto.dir/geometry.pb.cc.o"
  "CMakeFiles/pi_proto.dir/route.pb.cc.o"
  "CMakeFiles/pi_proto.dir/header.pb.cc.o"
  "CMakeFiles/pi_proto.dir/planning.pb.cc.o"
  "CMakeFiles/pi_proto.dir/chassis.pb.cc.o"
  "CMakeFiles/pi_proto.dir/perception_obstacle.pb.cc.o"
  "CMakeFiles/pi_proto.dir/decision.pb.cc.o"
  "CMakeFiles/pi_proto.dir/planning_internal.pb.cc.o"
  "CMakeFiles/pi_proto.dir/localization.pb.cc.o"
  "CMakeFiles/pi_proto.dir/operational_data.pb.cc.o"
  "libpi_proto.pdb"
  "libpi_proto.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/pi_proto.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
