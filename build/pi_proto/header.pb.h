// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: header.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_header_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_header_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3009000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3009000 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_header_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_header_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[1]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_header_2eproto;
namespace piauto {
namespace common {
class Header;
class HeaderDefaultTypeInternal;
extern HeaderDefaultTypeInternal _Header_default_instance_;
}  // namespace common
}  // namespace piauto
PROTOBUF_NAMESPACE_OPEN
template<> ::piauto::common::Header* Arena::CreateMaybeMessage<::piauto::common::Header>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace piauto {
namespace common {

// ===================================================================

class Header :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:piauto.common.Header) */ {
 public:
  Header();
  virtual ~Header();

  Header(const Header& from);
  Header(Header&& from) noexcept
    : Header() {
    *this = ::std::move(from);
  }

  inline Header& operator=(const Header& from) {
    CopyFrom(from);
    return *this;
  }
  inline Header& operator=(Header&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const Header& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const Header* internal_default_instance() {
    return reinterpret_cast<const Header*>(
               &_Header_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(Header& a, Header& b) {
    a.Swap(&b);
  }
  inline void Swap(Header* other) {
    if (other == this) return;
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline Header* New() const final {
    return CreateMaybeMessage<Header>(nullptr);
  }

  Header* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<Header>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const Header& from);
  void MergeFrom(const Header& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  #else
  bool MergePartialFromCodedStream(
      ::PROTOBUF_NAMESPACE_ID::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  ::PROTOBUF_NAMESPACE_ID::uint8* InternalSerializeWithCachedSizesToArray(
      ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(Header* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "piauto.common.Header";
  }
  private:
  inline ::PROTOBUF_NAMESPACE_ID::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_header_2eproto);
    return ::descriptor_table_header_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kModuleNameFieldNumber = 2,
    kTimestampFieldNumber = 1,
    kHardwareTimestampFieldNumber = 4,
    kSequenceNumFieldNumber = 3,
  };
  // string module_name = 2;
  void clear_module_name();
  const std::string& module_name() const;
  void set_module_name(const std::string& value);
  void set_module_name(std::string&& value);
  void set_module_name(const char* value);
  void set_module_name(const char* value, size_t size);
  std::string* mutable_module_name();
  std::string* release_module_name();
  void set_allocated_module_name(std::string* module_name);
  private:
  const std::string& _internal_module_name() const;
  void _internal_set_module_name(const std::string& value);
  std::string* _internal_mutable_module_name();
  public:

  // uint64 timestamp = 1;
  void clear_timestamp();
  ::PROTOBUF_NAMESPACE_ID::uint64 timestamp() const;
  void set_timestamp(::PROTOBUF_NAMESPACE_ID::uint64 value);

  // uint64 hardware_timestamp = 4;
  void clear_hardware_timestamp();
  ::PROTOBUF_NAMESPACE_ID::uint64 hardware_timestamp() const;
  void set_hardware_timestamp(::PROTOBUF_NAMESPACE_ID::uint64 value);

  // uint32 sequence_num = 3;
  void clear_sequence_num();
  ::PROTOBUF_NAMESPACE_ID::uint32 sequence_num() const;
  void set_sequence_num(::PROTOBUF_NAMESPACE_ID::uint32 value);

  // @@protoc_insertion_point(class_scope:piauto.common.Header)
 private:
  class _Internal;

  ::PROTOBUF_NAMESPACE_ID::internal::InternalMetadataWithArena _internal_metadata_;
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr module_name_;
  ::PROTOBUF_NAMESPACE_ID::uint64 timestamp_;
  ::PROTOBUF_NAMESPACE_ID::uint64 hardware_timestamp_;
  ::PROTOBUF_NAMESPACE_ID::uint32 sequence_num_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  friend struct ::TableStruct_header_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// Header

// uint64 timestamp = 1;
inline void Header::clear_timestamp() {
  timestamp_ = PROTOBUF_ULONGLONG(0);
}
inline ::PROTOBUF_NAMESPACE_ID::uint64 Header::timestamp() const {
  // @@protoc_insertion_point(field_get:piauto.common.Header.timestamp)
  return timestamp_;
}
inline void Header::set_timestamp(::PROTOBUF_NAMESPACE_ID::uint64 value) {
  
  timestamp_ = value;
  // @@protoc_insertion_point(field_set:piauto.common.Header.timestamp)
}

// string module_name = 2;
inline void Header::clear_module_name() {
  module_name_.ClearToEmptyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline const std::string& Header::module_name() const {
  // @@protoc_insertion_point(field_get:piauto.common.Header.module_name)
  return _internal_module_name();
}
inline void Header::set_module_name(const std::string& value) {
  _internal_set_module_name(value);
  // @@protoc_insertion_point(field_set:piauto.common.Header.module_name)
}
inline std::string* Header::mutable_module_name() {
  // @@protoc_insertion_point(field_mutable:piauto.common.Header.module_name)
  return _internal_mutable_module_name();
}
inline const std::string& Header::_internal_module_name() const {
  return module_name_.GetNoArena();
}
inline void Header::_internal_set_module_name(const std::string& value) {
  
  module_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), value);
}
inline void Header::set_module_name(std::string&& value) {
  
  module_name_.SetNoArena(
    &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:piauto.common.Header.module_name)
}
inline void Header::set_module_name(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  
  module_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:piauto.common.Header.module_name)
}
inline void Header::set_module_name(const char* value, size_t size) {
  
  module_name_.SetNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:piauto.common.Header.module_name)
}
inline std::string* Header::_internal_mutable_module_name() {
  
  return module_name_.MutableNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline std::string* Header::release_module_name() {
  // @@protoc_insertion_point(field_release:piauto.common.Header.module_name)
  
  return module_name_.ReleaseNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}
inline void Header::set_allocated_module_name(std::string* module_name) {
  if (module_name != nullptr) {
    
  } else {
    
  }
  module_name_.SetAllocatedNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), module_name);
  // @@protoc_insertion_point(field_set_allocated:piauto.common.Header.module_name)
}

// uint32 sequence_num = 3;
inline void Header::clear_sequence_num() {
  sequence_num_ = 0u;
}
inline ::PROTOBUF_NAMESPACE_ID::uint32 Header::sequence_num() const {
  // @@protoc_insertion_point(field_get:piauto.common.Header.sequence_num)
  return sequence_num_;
}
inline void Header::set_sequence_num(::PROTOBUF_NAMESPACE_ID::uint32 value) {
  
  sequence_num_ = value;
  // @@protoc_insertion_point(field_set:piauto.common.Header.sequence_num)
}

// uint64 hardware_timestamp = 4;
inline void Header::clear_hardware_timestamp() {
  hardware_timestamp_ = PROTOBUF_ULONGLONG(0);
}
inline ::PROTOBUF_NAMESPACE_ID::uint64 Header::hardware_timestamp() const {
  // @@protoc_insertion_point(field_get:piauto.common.Header.hardware_timestamp)
  return hardware_timestamp_;
}
inline void Header::set_hardware_timestamp(::PROTOBUF_NAMESPACE_ID::uint64 value) {
  
  hardware_timestamp_ = value;
  // @@protoc_insertion_point(field_set:piauto.common.Header.hardware_timestamp)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace common
}  // namespace piauto

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_header_2eproto
